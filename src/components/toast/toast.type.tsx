export type ToastObject = { message?: String; type?: String };
export type ItemObject = {
    key: number;
    message: string;
    type: string;
};

export interface ToastItemProps {
    child: (arg: (toastObj: ToastObject) => void) => void;
    dark?: boolean;
    message?: string;
    type?: string;
    timeout?: number;
    style?: React.CSSProperties;

    closeIcon?: boolean;
    dismissOnClick?: boolean;
}

export type ToastItem = {
    message?: string;
    dark?: boolean;
    type?: string;
    style?: React.CSSProperties;
    keyValue?: number;
    timeout?: number;
    // closeToast?: false | ((keyValue: any) => void);
    closeIcon?: boolean;
    closeToast?: boolean;
    onRest?: false | (() => void);
};

export type DarkStyle = {
    style?: React.CSSProperties;
};
