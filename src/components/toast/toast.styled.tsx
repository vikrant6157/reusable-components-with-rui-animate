import styled from 'styled-components';
import { fonts } from '../../constants';

const ToastContainer = styled.div`
    position: fixed;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    z-index: 100;
    right: 40px;
    bottom: 10px;
    width: 220px;
`;

const MasterContainer = styled.div`
    overflow: hidden;

    font-smooth: 10em;
    width: 210px;
`;

const MessageContainer = styled.div`
    position: relative;
    background: white;
    padding: 14px 10px;
    border: 1px solid gray;
    border-left: 4px solid #c0c0c0;
    border-radius: 12px;
    box-shadow: 0px 6px 20px rgba(0, 0, 0, 0.08);
    color: #ffffff;
    font-family: ${fonts.family.arial};
    cursor: pointer;
    width: 200px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-wrap: nowrap;
`;

const Message = styled.div`
    font-size: 14px;
    padding: 0 7px;
    color: black;
`;

const ToastIconContainer = styled.span`
    padding: 2px 2px 0;
    margin: 2px 2px 0;
    color: red;
    font-size: 18px;
`;
const CloseIconContainer = styled.div`
    position: absolute;
    right: 6px;
    top: 6px;
`;

export { ToastContainer, MasterContainer, MessageContainer, Message, ToastIconContainer, CloseIconContainer };
