import React, { useState, useEffect, useRef } from 'react';
import {
    AnimatedBlock,
    useMountedValue,
    interpolate,
    makeAnimatedComponent,
    AnimationConfigUtils,
} from 'react-ui-animate';
import { ItemObject, ToastObject, ToastItemProps, ToastItem } from './toast.type';
import './toast.module.css';
import { MasterContainer, MessageContainer, Message, CloseIconContainer, ToastIconContainer } from './toast.styled';
import { IoMdWarning, IoMdCheckmark } from 'react-icons/io';
import { BiErrorCircle } from 'react-icons/bi';

import { BsInfo } from 'react-icons/bs';
export { ToastContainer } from './toast.styled';

const MasterContainerAnimated = makeAnimatedComponent(MasterContainer);
const MessageContainerAnimated = makeAnimatedComponent(MessageContainer);

export const Box = ({ message, type, keyValue, style, timeout, onRest, closeToast, closeIcon, dark }: ToastItem) => {
    const [open, setOpen] = useState(true);

    const mv = useMountedValue(open, {
        from: 0,
        enter: 1,
        exit: 2,
        config: {
            ...AnimationConfigUtils.POWER2,
        },
    });

    const [toastProperties, setToastProperties] = useState({
        iconColor: '#5cb85c',
    });

    useEffect(() => {
        switch (type) {
            case 'success':
                setToastProperties({
                    iconColor: '#5cb85c',
                });
                break;
            case 'error':
                setToastProperties({
                    iconColor: '#ff2400',
                });
                break;
            case 'info':
                setToastProperties({
                    iconColor: '#008ecc',
                });
                break;
            case 'warning':
                setToastProperties({
                    iconColor: '#ffa500',
                });
                break;
            default:
                setToastProperties({
                    iconColor: '#5cb85c',
                });
        }

        setTimeout(() => {
            setOpen(false);
        }, timeout);
    }, [setOpen, type, timeout]);

    return (
        <div>
            {mv(
                (a, m) =>
                    m && (
                        <AnimatedBlock
                            style={{
                                width: 200,
                                height: interpolate(a.value, [0, 1, 2], [0, 80, 0]),
                                translateX: interpolate(a.value, [0, 1, 2], [0, 0, 230]),
                            }}
                        >
                            <MasterContainerAnimated>
                                <MessageContainerAnimated
                                    style={{
                                        ...style,
                                        border: dark ? `none` : ``,
                                        borderLeft: `4px solid ${toastProperties.iconColor}`,
                                        backgroundColor: dark ? `black` : `white`,
                                    }}
                                    onClick={() => closeToast && setOpen(false)}
                                >
                                    <ToastIconContainer>
                                        {type === 'success' && (
                                            <IoMdCheckmark style={{ color: toastProperties.iconColor }} />
                                        )}
                                        {type === 'error' && (
                                            <BiErrorCircle style={{ color: toastProperties.iconColor }} />
                                        )}
                                        {type === 'warning' && (
                                            <IoMdWarning style={{ color: toastProperties.iconColor }} />
                                        )}
                                        {type === 'info' && <BsInfo style={{ color: toastProperties.iconColor }} />}
                                    </ToastIconContainer>
                                    <Message style={{ color: dark ? `white` : `black` }}>
                                        {message ? message : type}
                                    </Message>
                                    {closeIcon && (
                                        <CloseIconContainer>
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 24 24"
                                                fill="red"
                                                width="12px"
                                                height="12px"
                                                onClick={(e) => setOpen(false)}
                                            >
                                                <path d="M0 0h24v24H0z" fill="none" />
                                                <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
                                            </svg>
                                        </CloseIconContainer>
                                    )}
                                </MessageContainerAnimated>
                            </MasterContainerAnimated>
                        </AnimatedBlock>
                    )
            )}
        </div>
    );
};

export const Toast = ({
    child,
    message,
    type,
    timeout = 5000,
    style,
    dark,
    closeIcon,
    dismissOnClick = true,
}: ToastItemProps) => {
    const toastId = useRef(0);
    const [items, setItems] = useState<Array<ItemObject>>([]);

    console.table(items);

    const onRest = (keyValue: number) => {
        setItems((prev) => prev.filter((each) => each.key !== keyValue));
    };

    useEffect(() => {
        child((toastObj: ToastObject) => {
            setItems((prev: any) => [
                ...prev,
                {
                    key: toastId.current++,
                    message: toastObj.message,
                    type: toastObj.type,
                },
            ]);
        });
    }, [child]);

    return (
        <div>
            {items.map((item, i) => (
                <Box
                    key={i}
                    keyValue={item.key}
                    message={item.message}
                    type={item.type}
                    timeout={timeout}
                    onRest={() => onRest(item.key)}
                    closeIcon={closeIcon}
                    closeToast={dismissOnClick}
                    style={style}
                    dark={dark}
                />
            ))}
        </div>
    );
};

export const useToast = () => {
    const ref = useRef<any>();

    return {
        handler: {
            child: (fn: (toastObj: ToastObject) => void) => (ref.current = fn),
        },
        toast: {
            success: (message?: String) => ref.current({ message, type: 'success' }),
            error: (message?: String) => ref.current({ message, type: 'error' }),
            warning: (message?: String) => ref.current({ message, type: 'warning' }),
            info: (message?: String) => ref.current({ message, type: 'info' }),
        },
    };
};
