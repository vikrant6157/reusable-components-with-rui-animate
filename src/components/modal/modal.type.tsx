export interface ModalProps {
    type?: String;
    children: React.ReactNode;
    visible: boolean;
    onOutsideClick?: (event: MouseEvent) => void;
    style?: Omit<React.CSSProperties, 'transform'>;
    isAnimated?: boolean;
    animationType?: 'elastic' | 'ease' | 'bounce' | 'wooble';
    disableScroll?: boolean;
    headerIcon?: React.ReactNode;
    header?: String;
    closeIcon?: (event: MouseEvent) => void;
    content?: String;
    contentAlign?: String;
    footer?: String;
    footerIcon?: React.ReactNode;
    button?: String;
    buttonAlign?: String;
    onOk?: (event: MouseEvent) => void;
    onCancel?: (event: MouseEvent) => void;
    buttonClick?: (event: MouseEvent) => void;
}
