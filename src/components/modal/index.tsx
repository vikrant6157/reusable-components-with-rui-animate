/* eslint-disable no-unused-vars */
import { useState, useEffect, useRef } from 'react';
import {
    useOutsideClick,
    makeAnimatedComponent,
    useMountedValue,
    interpolate,
    AnimationConfigUtils,
    UseAnimatedValueConfig,
} from 'react-ui-animate';
import { useScrollDisable } from '../../hooks';

import { AiOutlineClose } from 'react-icons/ai';

import { ModalProps } from './modal.type';
import {
    ContainerStyled,
    ModalHeaderStyled,
    ModalHeaderContentStyled,
    HeaderIconStyled,
    ModalBodyStyled,
    CloseIconStyled,
    ModalContentStyled,
    ModalContentContainerStyled,
    ModalFooterStyled,
    ModalFooterContentStyled,
    FooterIconStyled,
    ButtonSectionStyled,
    ButtonStyled,
} from './modal.styled';
import { FcApproval, FcInfo } from 'react-icons/fc';

const Container = makeAnimatedComponent(ContainerStyled);
const ModalBody = makeAnimatedComponent(ModalBodyStyled);
const ModalHeader = makeAnimatedComponent(ModalHeaderStyled);
const ModalContentContainer = makeAnimatedComponent(ModalContentContainerStyled);
const CloseIcon = makeAnimatedComponent(CloseIconStyled);
const ButtonSection = makeAnimatedComponent(ButtonSectionStyled);

const getAnimationConfig = (animationType: any) => {
    switch (animationType) {
        case 'elastic':
            return { ...AnimationConfigUtils.ELASTIC };
        case 'ease':
            return { ...AnimationConfigUtils.EASE_IN_OUT };
        case 'wooble':
            return { ...AnimationConfigUtils.WOOBLE };
        case 'bounce':
            return { ...AnimationConfigUtils.BOUNCE };

        default:
            return { ...AnimationConfigUtils.LINEAR };
    }
};

export const Modal = ({
    type,
    header,
    headerIcon,
    closeIcon,
    content,
    contentAlign,
    footer,
    footerIcon,
    button,
    buttonAlign,
    buttonClick,
    onOk,
    onCancel,
    children,
    visible,
    onOutsideClick,
    style,
    isAnimated = false,
    animationType,
    disableScroll = false,
}: ModalProps) => {
    const modalRef = useRef<HTMLElement>(null);

    const [alignStyle, setAlignStyle] = useState({
        justifyContent: 'center',
    });

    useEffect(() => {
        if (buttonAlign === 'left') {
            setAlignStyle({
                justifyContent: 'flex-start',
            });
        } else if (buttonAlign === 'right') {
            setAlignStyle({
                justifyContent: 'flex-end',
            });
        }
    }, [buttonAlign]);

    const [contentAlignStyle, setContentAlignStyle] = useState({
        alignItems: 'center',
    });

    useEffect(() => {
        if (contentAlign === 'left') {
            setContentAlignStyle({
                alignItems: 'flex-start',
            });
        } else if (contentAlign === 'right') {
            setContentAlignStyle({
                alignItems: 'flex-end',
            });
        }
    }, [animationType, contentAlign]);

    const animationConfig = getAnimationConfig(animationType);

    const config: UseAnimatedValueConfig = isAnimated ? animationConfig : { immediate: true };

    const transitions = useMountedValue(visible, {
        from: 0,
        enter: 1,
        exit: 0,
        config,
    });

    // Handle outside click
    useOutsideClick(modalRef, (e) => {
        e.stopPropagation();

        if (onOutsideClick && visible) onOutsideClick(e);
    });

    const closeIconClicked = (e: any) => {
        e.preventDefault();
        if (closeIcon) closeIcon(e);
    };

    const buttonClicked = (e: any) => {
        e.preventDefault();
        if (buttonClick) buttonClick(e);
    };

    const okClicked = (e: any) => {
        e.preventDefault();
        if (onOk) onOk(e);
    };

    const cancelClicked = (e: any) => {
        e.preventDefault();
        if (onCancel) onCancel(e);
    };

    useScrollDisable(disableScroll && visible);

    if (type === 'confirm') {
        return (
            <div>
                {transitions(
                    (animated, mounted) =>
                        mounted && (
                            <Container
                                style={{
                                    opacity: animated.value,
                                }}
                            >
                                <ModalBody
                                    ref={modalRef}
                                    style={{
                                        ...style,
                                        scale: interpolate(animated.value, [0, 1], [0.8, 1]),
                                    }}
                                >
                                    <ModalHeader>
                                        {headerIcon ? (
                                            <HeaderIconStyled>{headerIcon}</HeaderIconStyled>
                                        ) : (
                                            <HeaderIconStyled>
                                                <FcApproval />
                                            </HeaderIconStyled>
                                        )}

                                        <ModalHeaderContentStyled>
                                            {header ? { header } : `Confirmation Modal`}
                                        </ModalHeaderContentStyled>

                                        {closeIcon && (
                                            <CloseIcon
                                                style={{
                                                    position: 'absolute',
                                                    right: '3%',
                                                }}
                                                onClick={(e) => closeIconClicked(e)}
                                            >
                                                <AiOutlineClose />
                                            </CloseIcon>
                                        )}
                                    </ModalHeader>

                                    <ModalContentContainer
                                        style={contentAlign ? contentAlignStyle : { alignItems: 'flex-start' }}
                                    >
                                        {content ? (
                                            <ModalContentStyled>{content}</ModalContentStyled>
                                        ) : (
                                            <ModalContentStyled>Please confirm your action!</ModalContentStyled>
                                        )}

                                        <ModalContentStyled>{children}</ModalContentStyled>
                                    </ModalContentContainer>

                                    <ModalFooterStyled>
                                        <ButtonSection
                                            style={buttonAlign ? alignStyle : { justifyContent: 'flex-end' }}
                                        >
                                            {onCancel && (
                                                <ButtonStyled onClick={(e) => cancelClicked(e)} color="#ff0f00">
                                                    Cancel
                                                </ButtonStyled>
                                            )}
                                            {onOk && (
                                                <ButtonStyled onClick={(e) => okClicked(e)} color="lightgreen">
                                                    Ok
                                                </ButtonStyled>
                                            )}
                                        </ButtonSection>
                                        {footer && (
                                            <ModalFooterContentStyled>
                                                {footerIcon && <FooterIconStyled>{footerIcon}</FooterIconStyled>}
                                                {footer}
                                            </ModalFooterContentStyled>
                                        )}
                                    </ModalFooterStyled>
                                </ModalBody>
                            </Container>
                        )
                )}
            </div>
        );
    } else if (type === 'info') {
        return (
            <div>
                {transitions(
                    (animated, mounted) =>
                        mounted && (
                            <Container
                                style={{
                                    opacity: animated.value,
                                }}
                            >
                                <ModalBody
                                    ref={modalRef}
                                    style={{
                                        ...style,
                                        scale: interpolate(animated.value, [0, 1], [0.8, 1]),
                                    }}
                                >
                                    <ModalHeader>
                                        {headerIcon ? (
                                            <HeaderIconStyled>{headerIcon}</HeaderIconStyled>
                                        ) : (
                                            <HeaderIconStyled>
                                                <FcInfo />
                                            </HeaderIconStyled>
                                        )}

                                        <ModalHeaderContentStyled>
                                            {header ? { header } : `Info or Details Modal`}
                                        </ModalHeaderContentStyled>

                                        {closeIcon && (
                                            <CloseIcon
                                                style={{
                                                    position: 'absolute',
                                                    right: '3%',
                                                }}
                                                onClick={(e) => closeIconClicked(e)}
                                            >
                                                <AiOutlineClose />
                                            </CloseIcon>
                                        )}
                                    </ModalHeader>

                                    <ModalContentContainer
                                        style={contentAlign ? contentAlignStyle : { alignItems: 'flex-start' }}
                                    >
                                        {content ? (
                                            <ModalContentStyled>{content}</ModalContentStyled>
                                        ) : (
                                            <ModalContentStyled>Here are the details!</ModalContentStyled>
                                        )}

                                        <ModalContentStyled>{children}</ModalContentStyled>
                                    </ModalContentContainer>

                                    <ModalFooterStyled>
                                        <ButtonSection
                                            style={buttonAlign ? alignStyle : { justifyContent: 'flex-end' }}
                                        >
                                            {onOk && (
                                                <ButtonStyled onClick={(e) => okClicked(e)} color="lightblue">
                                                    Ok
                                                </ButtonStyled>
                                            )}
                                        </ButtonSection>
                                        {footer && (
                                            <ModalFooterContentStyled>
                                                {footerIcon && <FooterIconStyled>{footerIcon}</FooterIconStyled>}
                                                {footer}
                                            </ModalFooterContentStyled>
                                        )}
                                    </ModalFooterStyled>
                                </ModalBody>
                            </Container>
                        )
                )}
            </div>
        );
    }

    return (
        <div>
            {transitions(
                (animated, mounted) =>
                    mounted && (
                        <Container
                            style={{
                                opacity: animated.value,
                            }}
                        >
                            <ModalBody
                                ref={modalRef}
                                style={{
                                    ...style,
                                    scale: interpolate(animated.value, [0, 1], [0.8, 1]),
                                }}
                            >
                                {header && (
                                    <ModalHeaderStyled>
                                        {headerIcon && <HeaderIconStyled>{headerIcon}</HeaderIconStyled>}

                                        <ModalHeaderContentStyled>{header}</ModalHeaderContentStyled>

                                        {closeIcon && (
                                            <CloseIcon onClick={(e) => closeIconClicked(e)}>
                                                <AiOutlineClose />
                                            </CloseIcon>
                                        )}
                                    </ModalHeaderStyled>
                                )}

                                <ModalContentContainer style={contentAlignStyle}>
                                    {content && <ModalContentStyled>{content}</ModalContentStyled>}

                                    <ModalContentStyled>{children}</ModalContentStyled>
                                </ModalContentContainer>
                                <ModalFooterStyled>
                                    {button && (
                                        <ButtonSection style={alignStyle}>
                                            <ButtonStyled onClick={(e) => buttonClicked(e)} color="#C0C0C0">
                                                {button}
                                            </ButtonStyled>
                                        </ButtonSection>
                                    )}
                                    {footer && (
                                        <ModalFooterContentStyled>
                                            {footerIcon && <FooterIconStyled>{footerIcon}</FooterIconStyled>}
                                            {footer}
                                        </ModalFooterContentStyled>
                                    )}
                                </ModalFooterStyled>
                            </ModalBody>
                        </Container>
                    )
            )}
        </div>
    );
};
