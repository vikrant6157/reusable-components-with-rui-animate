import styled from "styled-components";

const ContainerStyled = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  z-index: 100;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.4);
  backdrop-filter: blur(5px);
`;

const ModalBodyStyled = styled.div`
  min-width: 400px;
  max-width: 80vw;
  max-height: 80vh;
  overflow-y: auto;
  padding: 2px;
  background: #ffffff;
  border-radius: 10px;
  font-family: Arial;
`;

const ModalHeaderStyled = styled.div`
  position: relative;
  padding: 20px 10px;
  margin: 5px;
  box-shadow: 0px 6px 46px rgba(0, 0, 0, 0.08);
  border-bottom: 1px solid rgba(0, 0, 0, 0.2);
  font-size: 18px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const HeaderIconStyled = styled.div`
  position: absolute;
  left: 1%;
  padding: 2px;
  margin: 3px 9px;
  font-size: 16px;
  height: 16px;
  width: 16px;
  border-radius: 20%;
`;

const ModalHeaderContentStyled = styled.div`
  position: absolute;
  left: 10%;
`;

const CloseIconStyled = styled.div`
  position: absolute;
  right: 3%;
  font-size: 16px;
  height: 16px;
  width: 16px;
  border: 2px solid #c0c0c0;

  border-radius: 20%;
  cursor: pointer;
  transition: 0.2s ease-in-out;
  &:hover {
    border: 2px solid red;
    color: red;
  }
`;

const ModalContentContainerStyled = styled.div`
  padding: 10px;
  margin: 5px;
  font-size: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const ModalContentStyled = styled.div`
  padding: 10px;
  margin: 5px;
  font-size: 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const ModalFooterStyled = styled.div`
  padding: 2px;
  margin: 2px;
  box-shadow: 0px 6px 46px rgba(0, 0, 0, 0.08);
  border-top: 1px solid rgba(0, 0, 0, 0.1);
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const ModalFooterContentStyled = styled.div`
  padding: 2px;
  margin: 2px;
  font-size: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const FooterIconStyled = styled.div`
  padding: 2px;
  margin: 2px;
  box-shadow: 0px 6px 46px rgba(0, 0, 0, 0.08);
  font-size: 10px;
  height: 10px;
  width: 10px;
  border-radius: 20%;
`;

const ButtonSectionStyled = styled.div`
  width: 90%;
  padding: 2px 4px;
  margin: 2px 4px;
  font-size: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ButtonStyled = styled.button`
  padding: 10px;
  margin: 2px;
  min-width: 70px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: none;
  border-radius: 10%;
  background-color: #dedede;
  cursor: pointer;
  transition: 0.15s ease-in-out;
  &:hover {
    background-color: ${({ color }) => color || "gray"};
  }
`;

export {
  ContainerStyled,
  ModalBodyStyled,
  ModalHeaderStyled,
  ModalHeaderContentStyled,
  HeaderIconStyled,
  CloseIconStyled,
  ModalContentStyled,
  ModalContentContainerStyled,
  ModalFooterStyled,
  ModalFooterContentStyled,
  FooterIconStyled,
  ButtonSectionStyled,
  ButtonStyled,
};
