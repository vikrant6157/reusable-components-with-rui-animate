import React, { useState } from 'react';

import { Modal } from './components';

const App = () => {
    const [visible, setVisible] = useState(false);

    const handleOk = () => {
        setVisible(false);
        console.log('Ok clicked');
    };

    return (
        <div>
            <button
                onClick={(e) => {
                    setVisible((prev) => !prev);
                }}
            >
                TOGGLE
            </button>
            <Modal visible={visible} type="info" onOk={handleOk}>
                This is Content from Children Component!
            </Modal>
        </div>
    );
};

export default App;
